package com.event.Wedding;


import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class wedding2 {

    private static final int SLIDESHOW_INTERVAL = 2000; // milliseconds
    private int imageIndex = 0;

    private final String[] imageUrls = {
        "hotel1.2.jpg",
        "hotel1.5.jpg",
        "hotel1.6.jpg",
        "hotel1.jpg"
    };

    public Scene createwedding2Scene(Stage primaryStage) {

        // Left side elements
        Label leftTitle = new Label("Marigold Banquets N Conventions");
        leftTitle.setStyle("-fx-font-size: 50px; -fx-font-weight: bold;");

        ImageView imageView = new ImageView();
        imageView.setFitWidth(1200);
        imageView.setFitHeight(1080);
        imageView.setPreserveRatio(true);
        updateImage(imageView);

        VBox leftVBox = new VBox(20);
        leftVBox.setPadding(new Insets(30, 0, 0, 0));
        leftVBox.setAlignment(Pos.TOP_CENTER);
        leftVBox.getChildren().addAll(leftTitle, imageView);

        // Right side elements
        Text desc1 = new Text("• Mimosa is a perfect venue designed to impress and create a beautiful experience and facilitate seamless execution. Come to Mimosa and reminisce the bond of companionship for generations to come.");
        desc1.setFont(new Font(25));
        desc1.setWrappingWidth(600);

        Text desc2 = new Text("• Open Spaces - 8000 sq. ft");
        desc2.setFont(new Font(25));
        desc2.setWrappingWidth(600);

        Text desc3 = new Text("• High Ceiling - 30 ft");
        desc3.setFont(new Font(25));
        desc3.setWrappingWidth(600);

        Text desc4 = new Text("• Covered Verandah - 2500 sq. ft");
        desc4.setFont(new Font(25));
        desc4.setWrappingWidth(600);

        Text desc5 = new Text("• People Approx - 1200 people");
        desc5.setFont(new Font(25));
        desc5.setWrappingWidth(600);

        Text desc6 = new Text("• Price");
        desc6.setFont(new Font(25));
        desc6.setWrappingWidth(600);

        VBox rightVBox = new VBox(70);
        rightVBox.setLayoutY(24);
        rightVBox.setPadding(new Insets(100, 0, 0, 0));
        rightVBox.getChildren().addAll(createRightBox(desc1, desc2, desc3, desc4, desc5, desc6));

        // Controls on the right side
        VBox controlsVBox = createControlsBox();

        // Back button
        Button backBtn = createBackButton();

        HBox topHBox = new HBox(backBtn);
        topHBox.setAlignment(Pos.TOP_LEFT);

        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(10));
        borderPane.setLeft(leftVBox);
        borderPane.setTop(topHBox);
        borderPane.setRight(rightVBox);

        Group root = new Group(borderPane);

        Scene scene = new Scene(root, 1800, 1000);

        // Slideshow animation
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(SLIDESHOW_INTERVAL), event -> updateImage(imageView)));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        return scene;
    }

    private void updateImage(ImageView imageView) {
        Image image = new Image(imageUrls[imageIndex]);
        imageView.setImage(image);
        imageIndex = (imageIndex + 1) % imageUrls.length;
    }

    private VBox createRightBox(Text desc1, Text desc2, Text desc3, Text desc4, Text desc5, Text desc6) {
        VBox box = new VBox(30, desc1, desc2, desc3, desc4, desc5, desc6);
        box.setPrefHeight(500);
        box.setPrefWidth(650);
        box.setStyle("-fx-border-color: black; -fx-border-width: 2; -fx-padding: 10; -fx-background-color: #f5f5f5;");
        return box;
    }

    private VBox createControlsBox() {
        Label selectd1 = new Label("Select Dates");
        selectd1.setFont(new Font(30));
        selectd1.setStyle("-fx-font-weight: bold;");

        DatePicker startDate = new DatePicker();
        startDate.setPromptText("Start Date");

        DatePicker endDate = new DatePicker();
        endDate.setPromptText("End Date");

        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll("Full Day", "Morning Section", "Evening Section");
        comboBox.setPromptText("Select Section");

        Button button = new Button("Book Now");
        button.setStyle("-fx-background-color: #ff4500; -fx-text-fill: white; -fx-font-size: 20px;");
        button.setOnMouseEntered(e -> button.setStyle("-fx-background-color: #ff6347; -fx-text-fill: white; -fx-font-size: 20px;"));
        button.setOnMouseExited(e -> button.setStyle("-fx-background-color: #ff4500; -fx-text-fill: white; -fx-font-size: 20px;"));

        VBox box = new VBox(20, selectd1, startDate, endDate, comboBox, button);
        box.setAlignment(Pos.CENTER);
        box.setStyle("-fx-background-color: rgba(255, 255, 255, 0.9); -fx-padding: 20;");
        return box;
    }

    private Button createBackButton() {
        Button backBtn = new Button("Back");
        backBtn.setStyle(
            "-fx-background-color: white; " +
            "-fx-text-fill: black; " +
            "-fx-font-size: 20px; " +
            "-fx-padding: 10 20 10 20; " +
            "-fx-background-radius: 5px;"+
            "-fx-border-color: black;"
        );

        backBtn.setOnMouseEntered(e -> backBtn.setStyle(
            "-fx-background-color: lightgray; " +
            "-fx-text-fill: black; " +
            "-fx-font-size: 20px; " +
            "-fx-padding: 10 20 10 20; " +
            "-fx-background-radius: 5px;"+
            "-fx-border-color: black;"
        ));

        backBtn.setOnMouseExited(e -> backBtn.setStyle(
            "-fx-background-color: white; " +
            "-fx-text-fill: black; " +
            "-fx-font-size: 20px; " +
            "-fx-padding: 10 20 10 20; " +
            "-fx-background-radius: 5px;"+
            "-fx-border-color: black;"
        ));

        return backBtn;
    }
}


