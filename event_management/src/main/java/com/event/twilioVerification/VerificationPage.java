package com.event.twilioVerification;

import com.event.Wedding.wedding1;
import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import com.twilio.rest.api.v2010.account.Message;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class VerificationPage {

    public static final String ACCOUNT_SID = "ACb195b7f13296599a4eabe54955d625f2";
    public static final String AUTH_TOKEN = "b10a41772e7a74acf2aea29991f722aa";
    public static final String VERIFY_SERVICE_SID = "VAbb24bb8c7595a46b43dcbbdcafc3ec93"; // replace with your Verify Service SID

    public Scene createVerificationScene(Stage verifyStage) {
        
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        // back button
        Button backButton = new Button("Back");
        backButton.setStyle("-fx-background-color: #ff6347; -fx-text-fill: white; -fx-font-size: 14px;-fx-font-size: 20px;");
        backButton.setOnAction(e -> {
            
            wedding1 wed1 = new wedding1();
            Scene wed1Scene = wed1.createwedding1Scene(verifyStage);
            verifyStage.setScene(wed1Scene);
        });

        

        Label phoneLabel = new Label("Phone Number:");
        phoneLabel.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;-fx-font-size: 20px;");
        GridPane.setConstraints(phoneLabel, 0, 0);

        TextField phoneInput = new TextField();
        phoneInput.setPromptText("+91XXXXXXXXXX");
        phoneInput.setStyle("-fx-padding: 5px; -fx-font-size: 14px;");
        GridPane.setConstraints(phoneInput, 1, 0);

        Button sendCodeButton = new Button("Send Code");
        sendCodeButton.setStyle("-fx-background-color: #4caf50; -fx-text-fill: white; -fx-font-size: 14px;");
        GridPane.setConstraints(sendCodeButton, 1, 1);

        Label codeLabel = new Label("Verification Code:");
        codeLabel.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;-fx-font-size: 20px;");
        GridPane.setConstraints(codeLabel, 0, 2);

        TextField codeInput = new TextField();
        codeInput.setStyle("-fx-padding: 5px; -fx-font-size: 14px;");
        codeInput.setPromptText("Verification Code");
        GridPane.setConstraints(codeInput, 1, 2);

        Button verifyCodeButton = new Button("Verify Code");
        verifyCodeButton.setStyle("-fx-background-color: #4caf50; -fx-text-fill: white; -fx-font-size: 14px;");
        GridPane.setConstraints(verifyCodeButton, 1, 3);

        grid.getChildren().addAll(phoneLabel, phoneInput, sendCodeButton, codeLabel, codeInput, verifyCodeButton);
        grid.setAlignment(Pos.CENTER);

        // hbox for back button
        HBox topHBox = new HBox();
        topHBox.setAlignment(Pos.TOP_LEFT);
        topHBox.getChildren().add(backButton);

        BorderPane root = new BorderPane();
        root.setTop(topHBox);
        root.setCenter(grid);

        sendCodeButton.setOnAction(e -> {
            String phoneNumber = phoneInput.getText();
            sendVerificationCode(phoneNumber);
        });

        verifyCodeButton.setOnAction(e -> {
            String phoneNumber = phoneInput.getText();
            String verificationCode = codeInput.getText();
            verifyCode(phoneNumber, verificationCode);
        });

        return new Scene(root, 300, 200);
    }

    private void sendVerificationCode(String phoneNumber) {
        Verification verification = Verification.creator(
                VERIFY_SERVICE_SID,
                phoneNumber,
                "sms"
        ).create();

        System.out.println("Sent verification code: " + verification.getSid());
    }

    private void verifyCode(String phoneNumber, String code) {
        try {
            VerificationCheck verificationCheck = VerificationCheck.creator(
                    VERIFY_SERVICE_SID
            ).setTo(phoneNumber)
             .setCode(code)
             .create();

            if ("approved".equals(verificationCheck.getStatus())) {
                System.out.println("Verification successful!");
                sendSms(phoneNumber, "Your event is successfully booked!");
            } else {
                System.out.println("Verification failed!");
            }
        } catch (Exception e) {
            System.err.println("Error verifying code: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void sendSms(String to, String body) {
        Message message = Message.creator(
                new com.twilio.type.PhoneNumber(to),
                new com.twilio.type.PhoneNumber("+17246232626"),
                body
        ).create();

        System.out.println("SMS sent: " + message.getSid());
    }
}
